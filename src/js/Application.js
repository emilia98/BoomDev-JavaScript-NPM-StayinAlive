import EventEmitter from "eventemitter3";
import Beat from "./Beat";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();

    const lyrics = ["Ah", "ha", "ha", "ha", "stayin' alive", "stayin' alive"];
    let count = 0;

    const message = document.createElement("div");
    message.classList.add("message");
    message.innerText = "Ah";
    count++;

    document.querySelector(".main").appendChild(message);

    this.emit(Application.events.READY);
    let main = document.querySelector(".main");

    this._beat = new Beat();
    this._beat.on(Beat.events.BIT, () => {
      let text = lyrics[count];
      let message = this._create(text);
      main.appendChild(message);
      count = count == lyrics.length - 1 ? 0 : count + 1;
    });
  }

  _create(text) {
    const message = document.createElement("div");
    message.classList.add("message");
    message.innerText = text;
    return message;
  }
}